import sys
sys.path.append('../')
from constants import *
import numpy
from plots import Plot


# Have albedo alpha vary
alpha_min = 0
alpha_max = 1
alpha = numpy.linspace(alpha_min, alpha_max, 500, endpoint=True)

# Calculating earth radii for different dates
radius_orbit_earth_max = 1.0167 * radius_orbit_earth
radius_orbit_earth_min = 0.9833 * radius_orbit_earth

# Calculating Solar Constants, all in Watt per square meter
solar_constant_venus = solar_constant_earth * (radius_orbit_earth / radius_orbit_venus) ** 2
solar_constant_mars = solar_constant_earth * (radius_orbit_earth / radius_orbit_mars) ** 2
solar_constant_earth_min = solar_constant_earth * (radius_orbit_earth / radius_orbit_earth_max) ** 2
solar_constant_earth_max = solar_constant_earth * (radius_orbit_earth / radius_orbit_earth_min) ** 2
print('S_venus={}'.format(round(solar_constant_venus)))
print('S_mars={}'.format(round(solar_constant_mars)))
print('S_earth_max={}'.format(round(solar_constant_earth_min)))
print('S_earth_max={}'.format(round(solar_constant_earth_max)))

# Calculating the temperatures according to Boltzmann equation and solar input
temp_earth = (solar_constant_earth/(4*sigma)*(1-alpha))**(1/4)
temp_earth_max = (solar_constant_earth_max/(4*sigma)*(1-alpha))**(1/4)
temp_earth_min = (solar_constant_earth_min/(4*sigma)*(1-alpha))**(1/4)
temp_venus = (solar_constant_venus/(4*sigma)*(1-alpha))**(1/4)
temp_mars = (solar_constant_mars/(4*sigma)*(1-alpha))**(1/4)

# Plotting temp over alpha for earth, mars, venus
plot = Plot()
plot.plot(alpha, temp_earth, 'Earth')
plot.plot(alpha, temp_venus, 'Venus')
plot.plot(alpha, temp_mars, 'Mars')
plot.x_label('albedo (unitless)')
plot.y_label('Temperature in K')
plot.save_fig('figures/earth_venus_mars.pdf')

# Plotting temp over alpha for earth on two different dates (different solar constants)
plot = Plot()
plot.plot(alpha, temp_earth_max, 'July 4')
plot.plot(alpha, temp_earth_min, 'January 5')
plot.x_label('albedo (unitless)')
plot.y_label('Temperature in K')
plot.save_fig('figures/earth_max_min.pdf')
