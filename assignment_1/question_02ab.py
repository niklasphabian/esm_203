import sys
sys.path.append('../')
from constants import *
import numpy
from plots import Plot, Plot3D

# Define a range for the emissivity
epsilon_min = 0.85
epsilon_max = 1
epsilon_range = numpy.linspace(epsilon_min, epsilon_max, 200, endpoint=True)

# Define a range for the albedo
alpha_min = 0.2
alpha_max = 0.4
alpha_range = numpy.linspace(alpha_min, alpha_max, 200, endpoint=True)


def t_s(epsilon, alpha, s=solar_constant_earth, h=H_avg, l=L_min):
    """ Return surface temperature of earth according to John Harte Model."""
    f = (2*W + s/4 * (3 - 3*alpha - 2*k_u - k_l) - h - 3*l/2) / (3 - 2*epsilon)
    t = numpy.power((f/sigma), 1/4) + zero_kelvin
    return t


def t_l(epsilon, alpha, s=solar_constant_earth, h=H_avg, l=L_min):
    """ Return lower layer temperature of earth according to John Harte Model."""
    f = (2*h*(1-epsilon)+s/4*(2*k_l + k_u) + l*3/2 + 2*W - 2*epsilon*(l + s/4*(alpha+k_u+k_l - 1)))/(3 - 2*epsilon)
    t = numpy.power((f / sigma / epsilon), 1/4) + zero_kelvin
    return t


def t_u(epsilon, alpha, s=solar_constant_earth, h=H_avg, l=L_min):
    """ Return upper layer temperature of earth according to John Harte Model."""
    f = (epsilon*(s/4*(1-alpha) + W) + (1 - epsilon)*(h + l*3/2 + W + s/4*(k_l + 2*k_u)))/(3 - 2*epsilon)
    t = numpy.power((f / sigma / epsilon), 1/4) + zero_kelvin
    return t


# Question a
plot = Plot()
plot.plot(epsilon_range, t_s(epsilon=epsilon_range, alpha=0.3), '$T_{s}$')
plot.plot(epsilon_range, t_l(epsilon=epsilon_range, alpha=0.3), '$T_{l}$')
plot.plot(epsilon_range, t_u(epsilon=epsilon_range, alpha=0.3), '$T_{u}$')
plot.xrange(x_min=epsilon_min, x_max=epsilon_max)
plot.x_label('$\epsilon$')
plot.y_label('$T_{s}$ in $^{\circ}\mathrm{C}$')
plot.save_fig('figures/question_2a.pdf')

# Question b
plot = Plot()
plot.plot(alpha_range, t_s(0.95, alpha_range), '$T_{s}$')
plot.plot(alpha_range, t_l(0.95, alpha_range), '$T_{l}$')
plot.plot(alpha_range, t_u(0.95, alpha_range), '$T_{u}$')
plot.xrange(x_min=alpha_min, x_max=alpha_max)
plot.x_label('$\\alpha_p$')
plot.y_label('$T_{s}$ in $^{\circ}\mathrm{C}$')
plot.save_fig('figures/question_2b.pdf')

# Question a + b
epsilon_mesh, alpha_mesh = numpy.meshgrid(epsilon_range, alpha_range)
plot3d = Plot3D()
plot3d.plot(epsilon_mesh, alpha_mesh, t_s(epsilon_mesh, alpha_mesh))
plot3d.x_label('$\\alpha_p$')
plot3d.y_label('$\epsilon$')
plot3d.z_label('$T_{s}$ in $^{\circ}\mathrm{C}$')
plot3d.view(x_angel=20, y_angel=120)
plot.save_fig('figures/question_2ab.pdf')

#plot3d.close()
