import sys
sys.path.append('../')
import sympy
import numpy
from assignment_1.question_02ab import t_s
from constants import *


def epsilon(co2):
    return 0.69947*numpy.power((co2), 0.047016)

# Question c part 1
print('Epsilon at 400 ppm is {0:.3f}'.format(epsilon(400)))
t_400 = t_s(epsilon=epsilon(400), alpha=0.28)
print('Temp at 400 ppm is {0:.3f} deg_c'.format(t_400))

alpha = sympy.Symbol("alpha")
F = sympy.Symbol("F")
H = H_min
L = L_min
epsilon = epsilon(450)
T = 17.23+273
f = '(2*W + solar_constant_earth/4 * (3 - 3*alpha - 2*k_u - k_l) - H - 3*L/2) / (3 - 2*epsilon)-sigma*T**4'
alpha = sympy.solve(f, alpha)
print(eval(str(alpha[0])))

# Question c part 2
solar_constant_earth = sympy.Symbol("solar_constant_earth")
alpha = sympy.Symbol("alpha")
sigma = sympy.Symbol("sigma")
epsilon = sympy.Symbol("epsilon")
H = sympy.Symbol("H")
L = sympy.Symbol("L")
W = sympy.Symbol("W")
k_u = sympy.Symbol("k_u")
k_l = sympy.Symbol("k_l")
T = sympy.Symbol("T")
alpha = sympy.solve(f, alpha)
print(eval(str(alpha)))
print(sympy.latex(eval(str(alpha))))




