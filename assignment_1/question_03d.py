import sys
sys.path.append('../')
from assignment_1.question_02ab import t_s
from constants import *

# Reference Earth temperature
t_ref = (t_s(epsilon=0.85, alpha=0.3, s=solar_constant_earth))

# Earth temperature with S0 = S0+2 W/m**2
t_2 = (t_s(epsilon=0.85, alpha=0.3, s=solar_constant_earth+2))
print(round(t_ref, 3), round(t_2, 3), round(t_ref-t_2, 3))

# Reference Earth temperature
t_ref = (t_s(epsilon=1, alpha=0.3, s=solar_constant_earth))

# Earth temperature with S0 = S0+2 W/m**2
t_2 = (t_s(epsilon=1, alpha=0.3, s=solar_constant_earth+2))
print(round(t_ref, 3), round(t_2, 3), round(t_ref-t_2, 3))
