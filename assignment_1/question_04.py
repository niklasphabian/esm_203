# Total heat capacity of the oceans
v_ocean = 1.3e18                    # m**3
v_ocean = 5e16                      # m**3
rho_water = 1000                    # kg/m**3
m_ocean = v_ocean * rho_water       # kg
c_p = 4186                          # J/kg/K
C_p = m_ocean * c_p                 # J/K
print('Total heat capacity of the oceans is {:.1e} J/K'.format(C_p))

# Annual excess heat
delta_s = 0.6                       # W/m**3
a_earth = 5.1e14                    # m**3
q = delta_s * a_earth               # W
Q = q * 365 * 24 * 60 * 60          # s
print('Total annual heat excess is {:.1e} J/year'.format(Q))

# Temperature change per year
T = Q/C_p                           # K/year
print('Total annual temp change is {} K'.format(round(T,3)))

# Time for 1 K to change
t = C_p/Q                           # K/year
print('Time for a 1 K temp change is {} years'.format(round(t,3)))



