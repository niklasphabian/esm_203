# Boltzmann constant
sigma = 5.67 * 10 ** (-8)

# Orbit distances, all in million km
radius_orbit_earth = 149.6
radius_orbit_venus = 108
radius_orbit_mars = 228

# Solar Constants, all in Watt per square meter
solar_constant_earth = 1362

# Energy generated from nonrenewable energy sources (fossil fuels + nuclear)
W = 0.021  # W per sqm

# Average latent heat flux from surface
L_min = 70  # W per sqm
L_max = 85  # W per sqm

# Average sensible heat flux from surface
H_min = 15  # W per sqm
H_avg = 20  # W per sqm
H_max = 25  # W per sqm

# Fraction of solar radiation absorbed in upper and lower layer of atmosphere
k_u = 0.18
k_l = 0.075

# 0 Kelvin
zero_kelvin = -273.15 # Celsius