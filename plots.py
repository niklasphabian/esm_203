import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm

font_size = 20

class Plot:
    def __init__(self):
        self.fig, self.ax = plt.subplots(nrows=1, ncols=1, figsize=(16, 10))
        self.setup_plot()
        self.legend = []

    def plot(self, x, y, series_name):
        ax = self.ax
        ax.plot(x, y, '-')
        self.legend.append(series_name)

    def setup_plot(self):
        self.ax.grid('on')
        self.ax.tick_params(labelsize=font_size, pad=10)

    def show(self):
        self.make_legend()
        plt.show()

    def save_fig(self, filename):
        self.make_legend()
        plt.savefig(filename)

    def x_label(self, x_label):
        self.ax.set_xlabel(x_label, fontsize=font_size, labelpad=20)

    def y_label(self, y_label):
        self.ax.set_ylabel(y_label, fontsize=font_size, labelpad=20)

    def make_legend(self):
        self.ax.legend(self.legend, fontsize=font_size)

    def xrange(self, x_min, x_max):
        self.ax.set_xlim([x_min, x_max])

    def close(self):
        plt.close(self.fig)


class Plot3D(Plot):
    def __init__(self):
        self.fig = plt.figure(figsize=(16, 10))
        self.ax = Axes3D(self.fig)
        self.setup_plot()
        self.color_bar = None
        self.legend = []

    def plot(self, x, y, z):
        surf = self.ax.plot_surface(x, y, z, antialiased=False, cmap=cm.coolwarm, linewidth=0.1)
        self.color_bar = self.fig.colorbar(surf, shrink=0.5, aspect=10)

    def z_label(self, z_label):
        self.ax.set_zlabel(z_label, fontsize=font_size, labelpad=20)
        self.color_bar.set_label(z_label, fontsize=font_size)

    def view(self, x_angel, y_angel):
        self.ax.view_init(x_angel, y_angel)


